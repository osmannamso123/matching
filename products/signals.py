from .models import Feed
from django.db.models.signals import post_save
from django.dispatch import receiver
from .tasks import add_products_from_feed


@receiver(post_save, sender=Feed)
def index_product(sender, instance, **kwargs):
    add_products_from_feed(instance)
