from django.contrib import admin
from .models import Feed, Product, Category, Brand, ProductImage


class ProductImageInline(admin.StackedInline):
    model = ProductImage
    fields = ('url', 'url_tag')
    readonly_fields = ('url_tag',)
    extra = 0


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'brand', 'category')
    inlines = (ProductImageInline,)
    search_fields = ('name',)
    list_filter = (
        'brand',
        'category'
    )


admin.site.register(Feed)
admin.site.register(Product, ProductAdmin)
admin.site.register(Category)
admin.site.register(Brand)
