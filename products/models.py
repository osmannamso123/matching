import os

from django.db import models
from django.db.models import ImageField
from django.utils.functional import cached_property
from django.utils.safestring import mark_safe

from matching.settings import BASE_DIR


class Category(models.Model):
    class Meta:
        verbose_name_plural = 'Categories'

    name = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.name


class Brand(models.Model):
    name = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255, default='')
    description = models.TextField(default='')
    brand = models.ForeignKey(Brand, on_delete=models.SET_NULL, null=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    media_url = models.CharField(max_length=1500, default='')


class ProductImage(models.Model):
    url = ImageField(upload_to='')
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    @cached_property
    def url_tag(self):
        if self.url:
            return mark_safe('<img src="/media/%s" width="250"/>' % (self.url,))
        else:
            return 'Upload it'
    url_tag.short_description = 'Image'


class Feed(models.Model):
    file = models.FileField()
