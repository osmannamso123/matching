from .models import Product, Category, Brand
import csv


def add_products_from_feed(feed):
    file_path = feed.file.path
    with open(file_path, mode='r', encoding='utf-8') as file:
        reader = csv.DictReader(file)
        for row in reader:
            category: Category
            if Category.objects.filter(name=row['category']).exists():
                category = Category.objects.get(name=row['category'])
            else:
                category = Category(name=row['category'])
                category.save()
            brand: Brand
            if Brand.objects.filter(name=row['brand']).exists():
                brand = Brand.objects.get(name=row['brand'])
            else:
                brand = Brand(name=row['brand'])
                brand.save()

            product = Product(name=row['name'],
                              description=row['description'],
                              brand=brand,
                              category=category,
                              media_url=row['images'])
            product.save()
